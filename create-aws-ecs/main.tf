provider "aws" {
    region = "${var.region}"
}
resource "aws_ecr_repository" "default" {
    name = "${var.create_repo}"
}
data "aws_ecs_task_definition" "main" {
    task_definition = "${aws_ecs_task_definition.main.family}"
}
resource "aws_ecs_task_definition" "main" {
    family                   = "${var.app_name}-${var.environment}"
    container_definitions    = <<DEFINITION
[
  {
    "cpu": 128,
                "portMappings":[
                {
                    "containerPort":${var.docker_port},
                    "hostPort":0,
                    "protocol":"tcp"
                }
            ],
    "environment": [{
      "name": "${var.app_name}"
    }],
    "essential": true,
    "image": "240167814999.dkr.ecr.sa-east-1.amazonaws.com/rc-${var.environment}/${var.app_name}:latest",
    "memory": 128,
    "memoryReservation": 64,
    "name": "${var.app_name}"
  }
]
DEFINITION
}

resource "aws_ecs_service" "main" {
  name            = "${var.app_name}"
  task_definition = "${aws_ecs_task_definition.main.family}:${max("${aws_ecs_task_definition.main.revision}", "${data.aws_ecs_task_definition.main.revision}")}"
  desired_count   = 2
  launch_type     = "EC2"
  cluster =       "${var.ecs_cluster_id}"
  load_balancer {
    target_group_arn = "${aws_alb_target_group.teste-terraform.arn}"
    container_name   = "${var.app_name}"
    container_port   = "${var.docker_port}"
  }
  depends_on = ["aws_alb_target_group.teste-terraform"]
}

resource "aws_alb_target_group" "teste-terraform" {
  name     = "${var.app_name}"
  port     = 9999
  protocol = "HTTP"
  vpc_id   = "${var.vpc}"
  target_type = "instance"
}

resource "aws_lb_listener" "teste-terraform" {
  load_balancer_arn = "${var.load_balancer}"
  port              = "9999"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.teste-terraform.arn}"
  }
}