variable "region" {
  description = "The name for the resource"
  default = "sa-east-1"
}
variable "iam_role" {
  description = "The env of the app"
  default = "ETL_BI"
}
variable "lambda_function_name" {
  description = "dsdss"
  type = "string"
  default = "regis"
}
variable "rest_api_name" {
  description = "rest api name"
  type = "string"
  default = "etl-bi"
}
variable "resource" {
  description = "sas"
  type        = "string"
  default     = "teste-regis"
}
variable "vpc" { 
  description = "VPC"
  type        = "string"
  default     = "vpc-0eed6c6b" 
}
variable "accountId" { 
  description = "sas"
  type        = "string"
  default     = "240167814999" 
}
variable "runtime" {
  description = "runtime lambda"
  type = "string"
  default = "python3.6"
}
