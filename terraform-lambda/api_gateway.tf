# The default provider configuration
provider "aws" {
  region = "us-east-1"
}

# Additional provider configuration for west coast region
provider "aws" {
  alias  = "sao-paulo"
  region = "sa-east-1"
}

data "aws_iam_role" "iam_role" {
  name = "${var.iam_role}"
}

data "aws_api_gateway_rest_api" "rest_api" {
  name = "${var.rest_api_name}"
}

data "archive_file" "zipit" {
  type        = "zip"
  source_file = "teste/lambda_function.py"
  output_path = "tf_lambda.zip"
}

resource "aws_api_gateway_resource" "regis-teste" {
  path_part   = "${var.resource}"
  parent_id   = "${data.aws_api_gateway_rest_api.rest_api.root_resource_id}"
  rest_api_id = "${data.aws_api_gateway_rest_api.rest_api.id}"
}

resource "aws_api_gateway_method" "method" {
  rest_api_id   = "${data.aws_api_gateway_rest_api.rest_api.id}"
  resource_id   = "${aws_api_gateway_resource.regis-teste.id}"
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_deployment" "test" {
  depends_on  = ["aws_api_gateway_integration.integration"]
  rest_api_id = "${data.aws_api_gateway_rest_api.rest_api.id}"
  stage_name  = "reginho"
}

/*-----------------*/
resource "aws_api_gateway_integration" "integration" {
  rest_api_id             = "${data.aws_api_gateway_rest_api.rest_api.id}"
  resource_id             = "${aws_api_gateway_resource.regis-teste.id}"
  http_method             = "${aws_api_gateway_method.method.http_method}"
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "${aws_lambda_function.test_lambda.invoke_arn}"
}

# Lambda
resource "aws_lambda_permission" "test_lambda" {
  provider = aws.sao-paulo
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.test_lambda.function_name}"
  principal     = "apigateway.amazonaws.com"
  source_arn = "arn:aws:execute-api:${var.region}:${var.accountId}:${data.aws_api_gateway_rest_api.rest_api.id}/*/${aws_api_gateway_method.method.http_method}${aws_api_gateway_resource.regis-teste.path}"
}

resource "aws_lambda_function" "test_lambda" {
  provider = aws.sao-paulo
  function_name = "${var.lambda_function_name}"
  filename      = "tf_lambda.zip"
  role          = "${data.aws_iam_role.iam_role.arn}"
  handler       = "lambda_function.lambda_handler"
  runtime       = "${var.runtime}"
  depends_on = [ "aws_cloudwatch_log_group.example"]
  }

resource "aws_cloudwatch_log_group" "example" {
  name              = "/aws/lambda/${var.lambda_function_name}"
  retention_in_days = 14
}
