# Configure a running logstash instance
class rcnginxphp::config::nginx(
  $cors_headers
) {
  $rcnginxphp::apps.each |Hash $app| {

    # Mapeia variaveis localmente
    $app_hostname = $app[app_hostname]
    $app_path = $app[app_path]
    $app_alias_hostname = $app[app_alias_hostname]
    $app_index = $app[app_index]
    $listen_options = $app[listen_options]

    if ( $rcnginxphp::https == true ) {
      nginx::resource::server { $app_hostname:
        www_root      => $app_path,
        server_name   => [ $app_hostname ] + $app_alias_hostname,
        listen_options => $listen_options,
        index_files   => [ $app_index ],
        try_files     => ['$uri', "/${app_index}\$is_args\$args"],
        format_log    => 'logstash_json',
        raw_append    => 'if ($blocked_bots = 1 ) { return 401; }',
        ssl           => true,
        ssl_cert      => '/etc/nginx/ssl/rentcars.self.crt',
        ssl_key       => '/etc/nginx/ssl/rentcars.self.key',
        ssl_dhparam   => '/etc/nginx/ssl/rentcars.self.pem',
        ssl_protocols => 'TLSv1.2',
        http2         => 'on',
        add_header    => $cors_headers
      }

      nginx::resource::location { $app_hostname:
        ssl                 => true,
        www_root            => $app_path,
        server              => $app_hostname,
        location            => "~ /${app_index}\$",
        fastcgi             => 'www',
        fastcgi_script      => '$document_root$fastcgi_script_name',
        location_cfg_append => {
          fastcgi_connect_timeout => '10s',
          fastcgi_read_timeout    => '5m',
          fastcgi_send_timeout    => '5m'
        },
      }

      # nginx::resource::location { "${app_hostname}-dot_php":
      #   server      => $app_hostname,
      #   ssl         => true,
      #   index_files => [],
      #   location    => "~ \.php\$",
      #   try_files   => ['$uri', "/index.php =406"],
      # }
    } else { # ($rcnginxphp::https === false)
      nginx::resource::server { $app_hostname:
        www_root    => $app_path,
        server_name => [ $app_hostname ] + $app_alias_hostname,
        listen_options => $listen_options,
        index_files => [ $app_index ],
        format_log  => 'logstash_json',
        raw_append  => 'if ($blocked_bots = 1 ) { return 401; }',
        try_files   => ['$uri', "/${app_index}\$is_args\$args"],
        ssl         => false,
        add_header    => $cors_headers
      }

      nginx::resource::location { $app_hostname:
        ssl                 => false,
        www_root            => $app_path,
        server              => $app_hostname,
        location            => "~ /${app_index}\$",
        fastcgi             => 'www',
        fastcgi_script      => '$document_root$fastcgi_script_name',
        location_cfg_append => {
          fastcgi_connect_timeout => '10s',
          fastcgi_read_timeout    => '5m',
          fastcgi_send_timeout    => '5m',
        }
      }

      # REDIRECTS
      $rcnginxphp::redirects.each |Hash $redirect| {
        $redirect_host = $redirect[host]

        if ($redirect_host == $app_hostname) {

          $from_path = $redirect[from_path]
          $to_path = $redirect[to_path]
          $redirect_code = $redirect[status]
          $hostname = $app_hostname
          $block_name = "${$hostname}_${from_path}"

          nginx::resource::location { $block_name:
            ensure              => present,
            server               => $hostname,
            location            => $from_path,
            location_custom_cfg => {
              'return' => "${redirect_code} ${$to_path}"
            }
          }
        }
      }

      # nginx::resource::location { "dot_php":
      #   server      => $app_hostname,
      #   ssl         => false,
      #   index_files => [],
      #   location    => "~ \.php\$",
      #   try_files   => ['$uri', "/index.php =406"],
      # }
    }
  }
}
