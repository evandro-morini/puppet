class rcnginxphp::config::logs{
  # Configura fluentd logs
  file { '/etc/td-agent/conf.d/10-source-nginx.conf':
    ensure  => file,
    mode    => '0644',
    content => template("${module_name}/nginx-logs.conf.erb"),
    notify  => Exec['restart-tdagent'], # Comes from rccommon
  }

  file { '/etc/td-agent/conf.d/10-source-phpfpm.conf':
    ensure  => file,
    mode    => '0644',
    content => template("${module_name}/php-logs.conf.erb"),
    notify  => Exec['restart-tdagent'], # Comes from rccommon
  }
}
