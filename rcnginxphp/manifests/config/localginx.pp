# Configure a running logstash instance
class rcnginxphp::config::localginx {

  # Default upstream for all
  nginx::resource::upstream { 'www':
    members => [
      '127.0.0.1:9000',
    ]
  }

  # Remove Default.conf
  file { '/etc/nginx/conf.d/default.conf':
    ensure => 'absent'
  }

  # SSL
  file { '/etc/nginx/ssl':
    ensure => 'directory',
  }~> file { '/etc/nginx/ssl/rentcars.self.crt':
    ensure  => file,
    mode    => '0644',
    content => template("${module_name}/rentcars.self.crt.erb"),
  }~> file { '/etc/nginx/ssl/rentcars.self.key':
    ensure  => file,
    mode    => '0644',
    content => template("${module_name}/rentcars.self.key.erb"),
  }~> file { '/etc/nginx/ssl/rentcars.self.pem':
    ensure  => file,
    mode    => '0644',
    content => template("${module_name}/rentcars.self.pem.erb"),
  }

  # BADBOT BLOCKER
  file { '/etc/nginx/conf.d/badbot_blocker.conf':
    ensure  => present,
    content => template("${module_name}/badbot_blocker.conf.erb"),
  }

  file { '/etc/nginx/conf.d/setip.conf':
    ensure  => present,
    content => template("${module_name}/setip.conf.erb")
  }

  nginx::resource::server { '00-localhost':
    server_name    => ['localhost'],
    format_log     => 'logstash_json',
  }

  nginx::resource::location { 'status':
    ensure      => present,
    location    => '/status',
    server      => '00-localhost',
    raw_append  => 'if ($blocked_bots = 1 ) { return 401; }',
    stub_status => true
  }

  nginx::resource::location { 'fpmstatus':
    ensure      => present,
    location    => '/fpmstatus',
    server      => '00-localhost',
    fastcgi     => 'www',
  }

  nginx::resource::location { '/':
    www_root            => '/var/www/html',
    server              => '00-localhost',
    location            => '/custom.php',
    fastcgi             => 'www',
    fastcgi_script      => '$document_root$fastcgi_script_name',
    location_cfg_append => {
      fastcgi_connect_timeout => '10s',
      fastcgi_read_timeout    => '5m',
      fastcgi_send_timeout    => '5m'
    }
  }

  file { ['/etc/nginx/sites-enabled/localhost.conf',
          '/etc/nginx/sites-avaliable/localhost.conf',]:
    ensure => absent,
  }

  file { ['/var/www',
          '/var/www/html',]:
    ensure => directory,
    mode   => '0755',
    owner  => 'nginx',
  }~> file { '/var/www/html/custom.php':
    ensure  => file,
    mode    => '0644',
    owner   => 'nginx',
    content => template("${module_name}/custom.php.erb"),
  }
}
