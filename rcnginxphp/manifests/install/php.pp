# Install PHP Class
class rcnginxphp::install::php($log_format) {
  $default_extensions = {
    'bcmath'       => {ini_prefix     => '20-',},
    'curl'         => {ini_prefix     => '20-',},
    'geoip'        => {ini_prefix     => '40-',},
    'mysqlnd'      => {ini_prefix     => '20-',},
    'mcrypt'       => {ini_prefix     => '20-',},
    'mbstring'     => {ini_prefix     => '20-',},
    'gd'           => {ini_prefix     => '20-',},
    'imap'         => {ini_prefix     => '20-',},
    'intl'         => {ini_prefix     => '20-',},
    'zip'          => {ini_prefix     => '40-',},
    igbinary => {
      ini_prefix     => '40-',
      package_prefix => 'php-pecl-',
      provider       => 'yum'
    },
    'ldap'         => {ini_prefix     => '20-',},
    'pdo'          => {ini_prefix     => '20-',},
    redis    => {
      ini_prefix     => '50-',
      package_prefix => 'php-pecl-',
      provider       => 'yum'
    },
    'pspell'       => {ini_prefix     => '20-',},
    'pgsql'        => {ini_prefix     => '20-',},
    'soap'         => {ini_prefix     => '20-',},
     imagick       => {
       ini_prefix     => '40-',
       package_prefix => 'php-pecl-',
       provider       => 'yum',
     },
    'json'         => {ini_prefix     => '40-',},
    'xmlrpc'       => {ini_prefix     => '30-',},
    'xml'          => {ini_prefix     => '20-',},
  }

  if ( $rcnginxphp::opcache == true ) {
    $opcache = {
      'opcache'       => {ini_prefix     => '40-',},
    }

    # Local mapping for use in ERB
    $opcache_mem_consumption = $rcnginxphp::opcache_mem_consumption
    $opcache_max_accelerated_files = $rcnginxphp::opcache_max_accelerated_files
    $opcache_max_wasted_percentage = $rcnginxphp::opcache_max_wasted_percentage

    file { '/etc/php.d':
      ensure => directory,
      mode   => '0644',
    }~> file { '/etc/php.d/40-opcache.ini':
      ensure  => file,
      mode    => '0644',
      content => template("${module_name}/opcache.ini.erb"),
    }
  } else { # $rcnginxphp::opcache == false
    $opcache = {}
    file { '/etc/php.d/40-opcache.ini':
      ensure  => absent,
    }
  }

  # Create the final extension list
  $extensions = $default_extensions + $opcache

  # PHP 7
  if ($rcnginxphp::version_php=='7.0') {
    class { '::php::repo::redhat':
      yum_repo => 'remi_php70' ,
    }-> class { '::php::globals':
      php_version => '7.0',
    }-> class { '::php':
          ensure          => present,
          fpm_user        => 'nginx',
          fpm_group       => 'nginx',
          manage_repos    => true,
          composer        => false,
          fpm             => true,
          pear            => true,
          dev             => true,
          log_owner       => 'nginx',
          phpunit         => false,
          settings        => {
            'Date/date.timezone'         => 'America/Sao_Paulo',
            'PHP/expose_php'             => 'off',
            'PHP/upload_max_filesize'    => '20M',
            'PHP/post_max_size'          => '20M',
            'PHP/memory_limit'           => "${rcnginxphp::php_memory}",
            'PHP/session.gc_maxlifetime' => '2592000',
          },
          fpm_pools       =>   { 'www' =>  {'pm_status_path'             => "/fpmstatus",
                                            'access_log'                 => "/var/log/php-fpm/access.log",
                                            'access_log_format'          => $log_format,
                                            'pm_start_servers'           => 20},
                         },
          extensions   => $extensions
      }
      # FIXME!!!
      # An special case for imagick... need to be installed by package on AMI Linux
    if $::facts['os']['name'] == 'Amazon' {
      Package { 'php70-pecl-imagick':
        ensure => installed,
      }
      Package { 'php70-gd':
        ensure => installed,
      }
    }
  } else { #($version_php != '7.0')
    # PHP5
    class { '::php':
      ensure          => present,
      fpm_user        => 'nginx',
      fpm_group       => 'nginx',
      manage_repos    => true,
      composer        => false,
      fpm             => true,
      pear            => true,
      dev             => true,
      log_owner       => 'nginx',
      phpunit         => false,
      settings        => {
        'Date/date.timezone'         => 'America/Sao_Paulo',
        'PHP/expose_php'             => 'off',
        'PHP/upload_max_filesize'    => '20M',
        'PHP/post_max_size'          => '20M',
        'PHP/memory_limit'           => "${rcnginxphp::php_memory}",
        'PHP/session.gc_maxlifetime' => '2592000',
      },
      fpm_pools       =>   { 'www' =>  {'pm_status_path'             => "/fpmstatus",
                                        'access_log'                 => "/var/log/php-fpm/access.log",
                                        'access_log_format'          => $log_format,
                                        'pm_start_servers'           => 20},
                     },
      extensions      => $extensions
    }
    # FIXME!!!
    # An special case for imagick... need to be installed by package on AMI Linux
    # Ugly... ya i know
    if $::facts['os']['name'] == 'Amazon' {
      Package { 'php56-pecl-imagick':
        ensure => installed,
      }

      Package { 'php56-gd':
        ensure => installed,
      }
    }
  }

  $remove_modules = ['40-memcache.ini', '10-opcache.ini', '50-memcached.ini', 'memcached.ini', 'imagick.ini', 'mbstring.ini',
                     'process.ini', 'bcmath.ini', 'imap.ini', 'mcrypt.ini', 'pspell.ini', 'curl.ini', 'intl.ini', 'redis.ini',
                     'gd.ini', 'jsonc.ini', 'mysqlnd.ini', 'soap.ini', 'geoip.ini', 'json.ini', 'pdo.ini', 'xml.ini', 'ldap.ini',
                     'igbinary.ini', 'pgsql.ini', 'xmlrpc.ini', 'opcache.ini','30-pdo_pgsql.ini', '20-json.ini']
  $remove_modules.each |$module| {
    File { "/etc/php.d/${module}":
      ensure => 'absent'
    }
  }
  File { '/etc/php.d/memcache.ini':
    ensure  => file,
    mode    => '0644',
    content => template("${module_name}/memcache.ini.erb"),
  }
  package { 'AmazonElastiCacheClusterClientPHP56-1.0.0-2.x86_64':
    ensure   => installed,
    provider => 'rpm',
    source   => 'https://s3.amazonaws.com/rentos/1/AmazonElastiCacheClusterClientPHP56-1.0.0-2.x86_64.rpm',
  }

  File { '/etc/php.d/20-pdopgsql.ini':
    ensure  => file,
    mode    => '0644',
    content => template("${module_name}/pgsql.ini.erb"),
    notify  => Exec['restart-phpfpm'],
  }

  # HOTFIXING
  # Amazon memcache need so.2 but centos have a newer
  if $operatingsystemrelease =~ /^7.*/ {
    file { '/lib64/libsasl2.so.2':
      ensure => 'link',
      target => '/lib64/libsasl2.so.3',
    }
  }


  exec { 'restart-phpfpm':
    command     => '/sbin/service php-fpm restart',
    refreshonly => true,
  }
}
