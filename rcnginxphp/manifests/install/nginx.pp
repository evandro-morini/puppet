# Configures nginx
class rcnginxphp::install::nginx($log_format){

  $gzip            = $rcnginxphp::gzip
  $gzip_comp_level = $rcnginxphp::gzip_comp_level
  $nginx_client_body_size = $rcnginxphp::nginx_client_body_size
  class { '::nginx':
    package_ensure  => latest,
    log_format      => {
      logstash_json => $log_format,
    },
    gzip            => $gzip,
    gzip_disable    => 'msie6',
    gzip_comp_level => $gzip_comp_level,
    gzip_min_length => 1100,
    gzip_buffers    => '16 8k',
    gzip_proxied    => 'any',
    gzip_types      => 'text/plain 	text/css	text/js	text/xml 	text/javascript 	application/javascript  	application/x-javascript
    	                  application/json 	application/xml  	application/rss+xml image/svg+xml',
    server_tokens   => off,
    client_max_body_size => $nginx_client_body_size,
    worker_processes => 'auto'
  }

}
