# Class for tests
# apps [
# {"app_hostname": "", "app_path": "", "app_alias_hostname": "", "app_index":""}
# ]

class rcnginxphp (
  $version_php = '5.6',
  $gzip = 'on',
  $gzip_comp_level = 6,
  $apps = [],
  $opcache = false,
  $https = false,
  $opcache_mem_consumption = 128,
  $opcache_max_accelerated_files = 20000,
  $opcache_max_wasted_percentage = 10,
  $php_memory = '256M',
  $redirects = [],
  $nginx_client_body_size = '10M'
  ) {

  include rcnginxphp::install::php
  include rcnginxphp::install::nginx

  include rcnginxphp::config::localginx # Base server config
  include rcnginxphp::config::nginx
  include rcnginxphp::config::logs
}
