provider "aws" {
  region = "${var.region}"
}

//criate repository - docker image
resource "aws_ecr_repository" "default" {
  name = "${var.create_repo}"

//upload image docker
provisioner "local-exec" {
    command = <<EOT
    cd ./docker-example && docker build -t 240167814999.dkr.ecr.sa-east-1.amazonaws.com/rc-${var.environment}/${var.app_name} .
    docker push 240167814999.dkr.ecr.sa-east-1.amazonaws.com/rc-${var.environment}/${var.app_name}:latest
    EOT
    }
}

//create task-definition
resource "aws_ecs_task_definition" "main" {
  family                   = "${var.app_name}-${var.environment}"
  container_definitions    = <<DEFINITION
[
  {
    "cpu": 128,
    "portMappings": [{
                    "containerPort": ${var.docker_port},
                    "hostPort": 0,
                    "protocol": "tcp"
    }],
    "environment": [{
      "name": "${var.app_name}"
    }],
    "essential": true,
    "image": "240167814999.dkr.ecr.sa-east-1.amazonaws.com/rc-${var.environment}/${var.app_name}:latest",
    "memory": 64,
    "memoryReservation": 36,
    "name": "${var.app_name}"
  }
]
DEFINITION
}
  
data "aws_ecs_task_definition" "main" {
  depends_on = [ "aws_ecs_task_definition.main" ]
  task_definition = "${aws_ecs_task_definition.main.family}"
}

//create service
resource "aws_ecs_service" "main" {
  name            = "${ecs_cluster_id}-${var.app_name}"
  task_definition = "${aws_ecs_task_definition.main.family}:${max("${aws_ecs_task_definition.main.revision}", "${data.aws_ecs_task_definition.main.revision}")}"
  desired_count   = 2
  launch_type     = "EC2"
  cluster         = "${var.ecs_cluster_id}"
  load_balancer {
  target_group_arn = "${aws_alb_target_group.application.arn}"
  container_name   = "${var.app_name}"
  container_port   = "${var.docker_port}"
  } 
  depends_on = ["aws_alb_target_group.application"]
}

//create target-group
resource "aws_alb_target_group" "application" {
  name            = "${var.app_name}"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc}"
  target_type = "instance"
    deregistration_delay = 30

  health_check {
    interval            = 25
    path                = "/"
    timeout             = 5
    healthy_threshold   = 2
    unhealthy_threshold = 3
    matcher             = 200
  }
}

data "aws_lb" "current" {
  name = "${var.load_balancer}"
}

data "aws_lb_listener" "selected80" {
  load_balancer_arn = "${data.aws_lb.current.arn}"
  port              = 80
}

output "name" {
  value = "${data.aws_lb_listener.selected80.arn}"
}

//create listerner rule
module "listener_rule_home" {
  source = "github.com/mergermarket/tf_alb_listener_rules"

  alb_listener_arn = "${data.aws_lb_listener.selected80.arn}"
  target_group_arn = "${aws_alb_target_group.application.arn}"

  host_condition    = "${var.domain}"
  starting_priority = "60"
}

# Select the correct zone from Route53
data "aws_route53_zone" "selected" {
  name         = "${var.environment_domain}."
  private_zone = false
}

//create domain in route53
resource "aws_route53_record" "www" {
  zone_id = "${data.aws_route53_zone.selected.zone_id}"
  name    = "${var.app_name}.${data.aws_route53_zone.selected.name}"
  type    = "A"

  alias {
    name                   = "${data.aws_lb.current.dns_name}"
    zone_id                = "${data.aws_lb.current.zone_id}"
    evaluate_target_health = true
  }
}

/*
data "http" "myip" {
  url = "http://ipv4.icanhazip.com"
}
*/