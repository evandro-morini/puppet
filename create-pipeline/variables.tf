variable "app_name" {
  description = "The name for the resource"
  default = ""
}
variable "region" {
  description = "The name for the resource"
  default = "sa-east-1"
}
variable "environment" {
  description = "The env of the app"
  default = ""
}
variable "docker_port" {
  description = "porta da aplicação"
  default = ""
}
variable "create_repo" {
  description = "cria o repositorio ECR"
  default     = ""
}
variable "ecs_cluster_id" {
  description = "The id of the ECS cluster"
  type        = "string"
  default     = ""
}
variable "load_balancer" { 
  description = "Load Balancer ID"
  type        = "string"
  default     = "" 
}
variable "vpc" { 
  description = "VPC"
  type        = "string"
  default     = "vpc-0eed6c6b" 
}

variable "domain" {
  type = "string"
  default = ""
}

variable "environment_domain" {
  type = "string"
  default = ""
}