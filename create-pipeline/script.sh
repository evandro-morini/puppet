 #!/bin/sh  
 # argumentos - mostra o valor das variáveis especiais  

curl -i -H "Authorization: token 637e7b39bb518ccf651bb2bcebd762a09d1013c9" \
    -d '{ 
        "name": "teste-devops",
        "auto_init": true,
        "private": true,
        "gitignore_template": "nanoc"
      }' \
    https://api.github.com/orgs/rentcars/repos

mkdir ./$2 && cd $_
git init
touch README.MD
git add README.MD
git commit -m 'initial commit'
#git remote add origin https://oauth2:637e7b39bb518ccf651bb2bcebd762a09d1013c9@github.com/rentcars/teste-devops.git
git remote add origin https://oauth2:$1@github.com/rentcars/teste-devops.git
git checkout -b stage
git push origin devops
git checkout -b integration
git push origin integration
git checkout -b production
git push origin production


#curl -X POST -u xxxx https://api.github.com/repos/rentcars/teste-devops/git/refs -d '{"ref":"refs/heads/stage", "sha":"'$SHA'"}


#SHA=$(curl https://api.github.com/repos/rentcars/teste-devops/git/refs/heads | grep -oP '(?<="sha": ")[^"]*'); &&
#            curl -X POST -H "Authorization: token 637e7b39bb518ccf651bb2bcebd762a09d1013c9" https://api.github.com/repos/rentcars/teste-devops/git/refs -d '{"ref":"refs/heads/stage", "sha":"'$SHA'"}'