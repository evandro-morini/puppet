class rclogs::config::cron {
  cron { 'Restart td-agent every hour':
    command => '/usr/sbin/service td-agent restart',
    user    => 'root',
    minute  => '*/10',
  }
}
