###
class rclogs::config::fluentd {

  File { '/etc/td-agent/conf.d/10-source-syslog.conf':
    ensure  => present,
    mode    => '0644',
    content => template("${module_name}/syslog.conf.erb"),
    notify  => Service['td-agent'],
  }

  File { '/etc/td-agent/conf.d/10-source-codedeploy-agent.conf':
    ensure  => present,
    mode    => '0644',
    content => template("${module_name}/codedeploy-agent.conf.erb"),
    notify  => Service['td-agent'],
  }

  $rclogs::logs.each |Hash $log| {
    # Mapeia variaveis localmente
    $log_name = $log[log_name]
    $log_path = $log[log_path]
    $log_format = $log[log_format]

    File { "/etc/td-agent/conf.d/10-source-${log_name}.conf":
      ensure  => present,
      mode    => '0644',
      content => template("${module_name}/source.conf.erb"),
      notify  => Service['td-agent'],
    }
  }

  File { '/etc/td-agent/conf.d/30-match-logs.conf':
    ensure  => present,
    mode    => '0644',
    content => template("${module_name}/match.conf.erb"),
    notify  => Service['td-agent'],
  }

  $rclogs::kinesis_matches.each |Hash $kstream| {

    $log_name = $kstream[log_name]
    $stream_name = $kstream[stream_name]
    $stream_region = $kstream[stream_region]
    $stream_type = $kstream[stream_type]

    if $stream_type == 'firehose' {
      File { "/etc/td-agent/conf.d/30-kinesis-match-${log_name}.conf":
        ensure  => present,
        mode    => '0644',
        content => template("${module_name}/knfh-match-firehose.conf.erb"),
        notify  => Service['td-agent'],
      }
    }

    if $stream_type == 'stream' {
      File { "/etc/td-agent/conf.d/30-kinesis-match-${log_name}.conf":
        ensure  => present,
        mode    => '0644',
        content => template("${module_name}/knfh-match-stream.conf.erb"),
        notify  => Service['td-agent'],
      }
    }
  }

  $rclogs::zabbix_matches.each |Hash $zabbix_conf| {

    $log_name = $zabbix_conf[log_name]
    $server = $zabbix_conf[server]
    $port = $zabbix_conf[port]
    $client_host = $zabbix_conf[client_host]
    $name_keys = $zabbix_conf[name_keys]

    File { "/etc/td-agent/conf.d/30-zabbix-match-${log_name}.conf":
      ensure  => present,
      mode    => '0644',
      content => template("${module_name}/zabbix-match.conf.erb"),
      notify  => Service['td-agent'],
    }
  }
}
