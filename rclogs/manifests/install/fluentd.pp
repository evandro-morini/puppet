##
class rclogs::install::fluentd {

  File { '/etc/yum.repos.d/td.repo':
    ensure  => present,
    mode    => '0644',
    content => template("${module_name}/td.repo.erb"),
  }~>
  Package { 'td-agent':
    ensure => latest,
  }~>
  File { '/etc/td-agent/td-agent.conf':
    ensure => present,
    mode => '0644',
    content => template("${module_name}/td-agent.conf.erb"),
  }~>
  file { '/etc/sysconfig/td-agent':
    ensure => present,
  }~>
  file_line { 'add conf':
    path => '/etc/sysconfig/td-agent',
    line => 'TD_AGENT_USER=root',
    notify  => Service['td-agent'],
  }~>
  Package { 'gelf':
    ensure   => installed,
    provider => fluentd_gem,
  }~>
  Package { 'fluent-plugin-gelf-hs':
    ensure   => installed,
    provider => fluentd_gem,
  }~>
  Package { 'fluent-plugin-input-gelf':
    ensure   => installed,
    provider => fluentd_gem,
  }~>
  Package { 'fluent-plugin-kinesis':
    ensure   => installed,
    provider => fluentd_gem,
    notify  => Service['td-agent'],
  }
  if ($rclogs::zabbix_matches.size > 0) {
    Package { 'fluent-plugin-zabbix':
      ensure   => installed,
      provider => fluentd_gem,
      notify  => Service['td-agent'],
    }
  }
  Service { 'td-agent':
    ensure     => running,
    hasrestart => true,
    enable     => true,
  }

  File { '/etc/cron.daily/td-agent.sh':
    ensure => absent,
    # mode => '0755',
    # content => template("${module_name}/td-agent.sh.erb"),
    # notify  => Exec['restart-tdagent'], # Comes from rccommon
  }

}
