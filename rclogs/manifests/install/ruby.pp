class rclogs::install::ruby {

include wget

exec {'ruby-x':
  environment => ["HOME=/home/centos/"],
  path        =>  [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/' ],
  command     => 'yum install -y openssl-devel gcc-c++ autoconf libtool git',
  }

wget::fetch { "download Google's index":
  source      => 'https://cache.ruby-lang.org/pub/ruby/2.4/ruby-2.4.6.tar.gz',
  destination => '/home/centos/',
  timeout     => 0,
  verbose     => false,
  }
wget::fetch { "gem":
  source      => 'https://rubygems.org/downloads/fluent-plugin-gelf-hs-1.0.0.gem',
  destination => '/home/centos/',
  }

wget::fetch { "input-gelf":
  source      => 'https://rubygems.org/downloads/fluent-plugin-input-gelf-0.2.0.gem',
  destination => '/home/centos/',
  }
  
exec {'ruby-install':
  environment => ["HOME=/home/centos/"],
  path        =>  [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/' ],
  creates     => '/home/centos/ruby-2.4.6/',
  command     => 'cd /home/centos/ && tar -xf /home/centos/ruby-2.4.6.tar.gz',
  }

exec {'gem-update':
  environment => ["HOME=/home/centos/"],
  path        =>  [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/' ],
  command     => 'cd /home/centos/ruby-2.4.6 && ./configure && make install && /home/centos/ruby-2.4.6/bin/gem install -v 2.4.6 rubygems-update',
  }

exec {'ruby-copy':
  environment => ["HOME=/home/centos/"],
  path        =>  [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/' ],
  command     => 'sudo cp -rf /home/centos/ruby-2.4.6/ruby /usr/bin/'
  }

exec {'gem-copy':
  environment => ["HOME=/home/centos/"],
  path        =>  [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/' ],
  command     => 'cp /home/centos/ruby-2.4.6/bin/gem /opt/td-agent/embedded/bin/ '
  }

exec {'protobuf-download':
  environment => ["HOME=/home/centos/"],
  path        =>  [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/' ],
  creates     => '/home/centos/protobuf/',
  command     => 'cd /home/centos/ && git clone https://github.com/protocolbuffers/protobuf.git'
  }

exec {'protobuf-install':
  environment => ["HOME=/home/centos/"],
  path        =>  [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/' ],
  command     => 'cd /home/centos/protobuf && ./autogen.sh && ./configure'
  }

exec {'gem-gelf':
  environment => ["HOME=/home/centos/"],
  path        =>  [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/' ],
  command     => '/opt/td-agent/embedded/bin/gem install gelfd2 fluentd fluent-plugin-kinesis',
  notify  => Service['td-agent'],
  }

exec {'gem-fluentd':
  environment => ["HOME=/home/centos/"],
  path        =>  [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/' ],
  creates     => '/home/centos/fluent-plugin-gelf-hs-1.0.0.gem',
  command     => '/opt/td-agent/embedded/bin/gem install --local fluent-plugin-gelf-hs-1.0.0.gem'
  }

exec {'gem-fluentd-new':
  environment => ["HOME=/home/centos/"],
  path        =>  [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/' ],
  creates     => '/home/centos/fluent-plugin-input-gelf-0.2.0.gem',
  command     => '/opt/td-agent/embedded/bin/gem install --local fluent-plugin-input-gelf-0.2.0.gem'
  }
}
