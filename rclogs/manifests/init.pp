# Class for tests
# Default format of logs
# [{"log_name":"","log_path":"","log_format":""}]
class rclogs (
  $logs = [],
  $kinesis_matches = [],
  $zabbix_matches = []){

  include rclogs::install::ruby
  include rclogs::install::fluentd
  include rclogs::config::fluentd
  include rclogs::config::cron
}
